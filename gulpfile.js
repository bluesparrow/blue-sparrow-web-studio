var gulp = require('gulp');
var sass = require('gulp-sass');

var uglify = require('gulp-uglify');
var concat = require('gulp-concat');
var rename = require('gulp-rename');

var plumber = require('gulp-plumber');
var notify = require("gulp-notify");
var imagemin = require('gulp-imagemin');
var pngquant = require('imagemin-pngquant');
//var combineMq = require('gulp-combine-mq');
var minifyCss = require('gulp-minify-css');

var connect = require('gulp-connect-php');
var browserSync = require('browser-sync');
 

// Compile sass into CSS & auto-inject into browsers
gulp.task('sass', function() {
  return gulp.src("components/stylesheets/**/*.scss")
    .pipe(plumber({errorHandler: notify.onError("Error: <%= error.message %>")}))
    .pipe(sass({sourceComments: false, outputStyle: 'compressed'}))
    .pipe(gulp.dest("css"))
    .pipe(browserSync.stream());
});

// Uglify task
gulp.task('uglify', function() {
  return gulp.src('components/scripts/*.js')
    .pipe(plumber({errorHandler: notify.onError("Error: <%= error.message %>")}))
    .pipe(uglify())
    .pipe(gulp.dest('js'));
});

// Uglify Plugins into one file
gulp.task('uglifyPlugins', function() {
  return gulp.src(['components/libs/jQuery-Form-Validator/form-validator/jquery.form-validator.js', 
    'components/libs/jQuery-Form-Validator/form-validator/security.js', 
    'components/libs/owlcarousel/owl-carousel/owl.carousel.js', 
    'components/libs/pushy/js/pushy.js', 
    'components/libs/simpleParallax/js/jquery.simpleparallax.js', 
    'components/libs/smooth-scroll.js/dist/js/smooth-scroll.js', 
    'components/libs/retina.js/dist/retina.js',
    'components/libs/slimScroll/jquery.slimscroll.js'])
    .pipe(concat('plugins.js'))
    .pipe(gulp.dest('js'))
    .pipe(rename('plugins.min.js'))
    .pipe(plumber({errorHandler: notify.onError("Error: <%= error.message %>")}))
    .pipe(uglify())
    .pipe(gulp.dest('js'));
});

// Static Server + watching scss/php files
gulp.task('watch', function() {

  connect.server({keepalive: true}, function (){
    browserSync({
      proxy: 'localhost:8000',
      notify: true
    });
  });

  gulp.watch("components/scripts/*.js", ['uglify']);
  gulp.watch("components/stylesheets/**/*.scss", ['sass']);
  gulp.watch("**/*.php").on('change', browserSync.reload);
});


// Minify Images
gulp.task('imgmin', function () {
  return gulp.src('srcimg/*')
      .pipe(imagemin({
          progressive: true,
          svgoPlugins: [{removeViewBox: false}],
          use: [pngquant()]
      }))
      .pipe(gulp.dest('images'));
});

// Combine MQ
// gulp.task('combineMq', function () {
//   return gulp.src('css/styles.css')
//   .pipe(combineMq({
//       beautify: true
//   }))
//   .pipe(gulp.dest('css/style'));
// });

// Minify Plugins CSS
gulp.task('minifyPlugins', function() {
  return gulp.src(['components/libs/pushy/css/pushy.css', 
    'components/libs/owlcarousel/owl-carousel/owl.carousel.css', 
    'components/libs/animate.css/animate.css'])
    .pipe(concat('plugins.css'))
    .pipe(gulp.dest('css'))
    .pipe(rename('plugins.min.css'))
    .pipe(minifyCss({compatibility: 'ie8'}))
    .pipe(gulp.dest('css'));
});

gulp.task('build', ['imgmin', 'uglifyPlugins', 'minifyPlugins']);
gulp.task('default', ['build', 'watch']);