# Blue Sparrow web studio

This README outlines the details of collaborating on this Website.

## Prerequisites

You will need the following things properly installed on your computer.

* [Git](http://git-scm.com/)
* [Node.js](http://nodejs.org/) (with NPM) 
* [Bower](http://bower.io)
* [Gulp](https://github.com/gulpjs/gulp/blob/master/docs/getting-started.md)
* [Ruby](http://rubyinstaller.org)
* [Sass](http://sass-lang.com)


## Installation

* `git clone <repository-url>` this repository
* `npm install -g gulp`
* `npm install -g bower`
* `npm install && bower install`


## Running / Development

* `gulp watch`
* `gulp build` to clean code and optimize image at the end.