<?php 
  session_start();

  if(isset($_POST['name']) && isset($_POST['email']) && isset($_POST['message']) && isset($_POST['captcha']) && isset($_SESSION['captcha']) ):
    $name = strip_tags($_POST['name']);
    $email = strip_tags($_POST['email']);
    $phone = strip_tags($_POST['phone']);
    $website = strip_tags($_POST['website']);

    $deadline = $_POST['deadline'];
    $budget = $_POST['budget'];

    $skills = join(", ", $_POST["skills"]);
    
    $message = htmlentities($_POST['message']);

    $message_body = "<html><body>";
    $message_body .= "<div style='overflow:hidden; background: #1e97c2; text-align: center;'><img src='http://bluesparrowstudio.com/images/bsemail.jpg' style='max-width: 100%;'></div>";
    $message_body .= "<table style='border: 1px solid #eee; border-collapse: collapse; border-spacing: 0; width: 100%'>";
    $message_body .= "<tr style='background-color: #f4f4f4'><td colspan='2' style='border: 1px solid #eee; padding: 5px; border-collapse: collapse; border-spacing: 0;'><h4 style='margin: 0;'>Client Details:</h4></td></tr>";
    $message_body .= "<tr>";
    $message_body .= "<td style='border: 1px solid #eee; padding: 5px; border-collapse: collapse; border-spacing: 0;'><strong>Name:</strong> ".$name."</td>";
    $message_body .= "<td style='border: 1px solid #eee; padding: 5px; border-collapse: collapse; border-spacing: 0;'><strong>Email:</strong> ".$email."</td>";
    $message_body .= "</tr>";
    $message_body .= "<tr>";
    $message_body .= "<td style='border: 1px solid #eee; padding: 5px; border-collapse: collapse; border-spacing: 0;'><strong>Phone:</strong> ".$phone."</td>";
    $message_body .= "<td style='border: 1px solid #eee; padding: 5px; border-collapse: collapse; border-spacing: 0;'><strong>Website:</strong> ".$website."</td>";
    $message_body .= "</tr>";

    $message_body .= "<tr style='background-color: #f4f4f4'><td colspan='2' style='border: 1px solid #eee; padding: 5px; border-collapse: collapse; border-spacing: 0;'><h4 style='margin: 0;'>Estimates:</h4></td></tr>";
    $message_body .= "<tr>";
    $message_body .= "<td style='border: 1px solid #eee; padding: 5px; border-collapse: collapse; border-spacing: 0;'><strong>Deadline:</strong> ".$deadline."</td>";
    $message_body .= "<td style='border: 1px solid #eee; padding: 5px; border-collapse: collapse; border-spacing: 0;'><strong>Budget:</strong> ".$budget."</td>";
    $message_body .= "</tr>";

    $message_body .= "<tr style='background-color: #f4f4f4'><td colspan='2' style='border: 1px solid #eee; padding: 5px; border-collapse: collapse; border-spacing: 0;'><h4 style='margin: 0;'>Required Skills:</h4></td></tr>";
    $message_body .= "<tr><td colspan='2' style='border: 1px solid #eee; padding: 5px; border-collapse: collapse; border-spacing: 0;'>".$skills."</td></tr>";

    $message_body .= "<tr style='background-color: #f4f4f4'><td colspan='2' style='border: 1px solid #eee; padding: 5px; border-collapse: collapse; border-spacing: 0;'><h4 style='margin: 0;'>Details:</h4></td></tr>";
    $message_body .= "<tr><td colspan='2' style='border: 1px solid #eee; padding: 5px; border-collapse: collapse; border-spacing: 0;'>".$message."</td></tr>";
    $message_body .= "<tr style='background-color: #f4f4f4'><td colspan='2' style='border: 1px solid #eee; padding: 10px 5px; border-collapse: collapse; border-spacing: 0; font-style: italic; font-weight: bold;'>IP Address: " .$_SERVER['REMOTE_ADDR']."</td></tr>";
    $message_body .= "</table>";
    $message_body .= "</body></html>";
    
    $to = 'monkeytempal@gmail.com';
    $subject = 'New Project Request in Blue Sparrow Web Studio';
    $body = $message_body;

    $headers = "From: " . $name.'<'.$email.'>' . "\r\n";
    $headers .= "Reply-To: ". $email . "\r\n";
    $headers .= "CC: ". $email . "\r\n";
    $headers .= "MIME-Version: 1.0\r\n";
    $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";

    if(empty($name) && empty($email) && empty($message)){
      $error = '<strong>Error - </strong> Please fill in required fields.';  
    }elseif(!isset($deadline) && !isset($budget)){
      $error = '<strong>Error - </strong> Please select your budget and deadlines.';  
    }elseif(!filter_var($email, FILTER_VALIDATE_EMAIL)){
      $error = '<strong>Error - </strong> Please use valid email address.'; 
    }elseif( $_POST['captcha'] != ($_SESSION['captcha'][0]+$_SESSION['captcha'][1]) ) {
      $error = "<strong>Error - </strong> Invalid captcha answer";
    }else{
      mail($to, $subject, $body, $headers);
      $success = 'Your message has been sent, we will get back to you as soon as possible.';
    }
  endif;

  $_SESSION['captcha'] = array( mt_rand(0,9), mt_rand(1, 9) );
?>

<!doctype html>
<html class="no-js" lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<title>Get a Quote - Blue Sparrow web studio</title>
		<meta name="description" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1">

		<!-- Place favicon icons -->
		<link rel="icon" type="image/x-icon" href="images/favicon.ico" />
		<link rel="icon" type="image/png" href="images/touch-icon-iphone.png" />
		<link rel="apple-touch-icon" href="images/touch-icon-iphone.png">
		<link rel="apple-touch-icon" sizes="76x76" href="images/touch-icon-76.png">
		<link rel="apple-touch-icon" sizes="120x120" href="images/touch-icon-120.png">
		<link rel="apple-touch-icon" sizes="152x152" href="images/touch-icon-152.png">
		<link rel="apple-touch-icon" sizes="180x180" href="images/touch-icon-iphone-180.png">

		<!-- Font and Dependencies -->
		<link rel="stylesheet" href="css/bootstrap.min.css">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
		<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Source+Sans+Pro:400,400italic,700,700italic,300,300italic">
		<link rel="stylesheet" href="css/plugins.min.css">

		<!-- Custom styles -->
		<link rel="stylesheet" href="css/styles.css">

		<!-- Initiate Modernizr -->
		<script async src="js/vendor/modernizr-2.8.3.min.js"></script>
	</head>

  <body class="page quote">
    <?php if(isset($error)){ 
      echo '<div class="alert-msg error">
              <div class="container">
                '.$error.'
                <span class="close">&times;</span>
              </div>
            </div>';
    }
    ?>

    <?php if(isset($success)){ 
      echo '<div class="alert-msg success">
              <div class="container">
                '.$success.'
                <span class="close">&times;</span>
              </div>
            </div>';
    }
    ?>

    <div id="mainframe">
      <?php include('partials/page-header.php'); ?>
      
      <section id="page-banner" class="parallax" data-speed="0.5">
        <?php include('partials/page-banner.php'); ?>
      </section>

      <?php include('partials/quote-form.php'); ?>
    </div>
    <!-- END Mainframe -->

    <?php include('partials/preload.php'); ?>
    <!-- END Preloader -->

    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="js/vendor/jquery.min.js"><\/script>')</script>

    <!-- Bootstrap JS -->
    <script src="js/bootstrap.min.js"></script>
    <script src="js/plugins.min.js"></script>

    <!-- Custom Scripts -->
    <script src="js/main.js"></script>
  </body>
</html>