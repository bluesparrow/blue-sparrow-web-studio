<header id="header">
	<nav class="navbar navbar-default navbar-fixed-top navbar-page">
		<div class="container">
			<!-- Brand and toggle get grouped for better mobile display -->
			<div class="navbar-header">
				<a class="navbar-brand" href="/">
					<span class="brand-name">Blue Sparrow Web Studio</span> <br> 
					<small class="slogan">a creative agency</small>
				</a>
			</div>
		
			<!-- Collect the nav links, forms, and other content for toggling -->
			<ul class="nav navbar-nav navbar-right">
				<li><a href="/" class="btn btn-sm btn-brand" data-hover="Homepage">Homepage</a></li>
			</ul>
		</div>
	</nav>
</header>
<!-- END header -->