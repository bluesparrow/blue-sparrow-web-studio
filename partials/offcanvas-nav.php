<!-- Pushy Menu -->
    <nav class="pushy pushy-left">
      <ul class="list-unstyled">
        <li><a data-scroll href="#">Home</a></li>
        <li><a data-scroll href="#about">About us</a></li>
        <li><a data-scroll href="#services">Our services</a></li>
        <li><a data-scroll href="#testimonials">Testimonials</a></li>
        <li><a href="quote">Start a project</a></li>
      </ul>
    </nav>

    <!-- Site Overlay -->
    <div class="site-overlay"></div>