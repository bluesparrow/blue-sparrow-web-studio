<section id="quote-form">
        <div class="container">
          <form id="quote" class="row" action="/quote" method="post">
            <div class="clearfix">
              <div class="col-sm-4">
                <div class="form-group">
                  <input type="text" class="form-control" placeholder="Name" name="name" data-validation="required">
                </div>
              </div>

              <div class="col-sm-4">
                <div class="form-group">
                  <input type="email" class="form-control" placeholder="Email" name="email" data-validation="email">
                </div>
              </div>

              <div class="col-sm-4">
                <div class="form-group">
                  <input type="tel" class="form-control" placeholder="Phone (optional)" name="phone">
                </div>
              </div>
            </div>

            <div class="clearfix">
              <div class="col-sm-4">
                <div class="form-group">
                  <input type="text" class="form-control" placeholder="Website (optional)" name="website">
                </div>
              </div>

              <div class="col-sm-4">
                <div class="form-group">
                  <select name="deadline" class="form-control" data-validation="required">
                    <option value="" selected disabled>Deadline</option>
                    <option>Within a week</option>
                    <option>In a month</option>
                    <option>In 2 months</option>
                    <option>Not sure</option>
                  </select>
                </div>
              </div>

              <div class="col-sm-4">
                <div class="form-group">
                  <select name="budget" class="form-control" data-validation="required">
                    <option value="" selected disabled>Budget</option>
                    <option>$100 - $500</option>
                    <option>$500 - $1000</option>
                    <option>above $1000</option>
                  </select>
                </div>
              </div>
            </div>

            <div class="form-group skills-container">
              <div class="col-sm-12">
                <h4>Select skills you need</h4>
              </div>

              <div class="col-sm-3">
                <label>
                  <input type="checkbox" name="skills[frontend]" value="Frontend Development">
                  Frontend Development
                </label>

                <label>
                  <input type="checkbox" name="skills[webdesign]" value="Web Design">
                  Website Design
                </label>
              </div>

              <div class="col-sm-3">
                <label>
                  <input type="checkbox" name="skills[psdtohtml]" value="PSD to HTML">
                  PSD to HTML
                </label>

                <label>
                  <input type="checkbox" name="skills[graphics]" value="Graphics Design">
                  Graphics Design
                </label>
              </div>

              <div class="col-sm-3">
                <label>
                  <input type="checkbox" name="skills[backend]" value="Backend Development">
                  Backend Development
                </label>

                <label>
                  <input type="checkbox" name="skills[logo]" value="Logo Design">
                  Logo Design
                </label>
              </div>

              <div class="col-sm-3">
                <label>
                  <input type="checkbox" name="skills[more]" value="I need more">
                  I want something more
                </label>
              </div>
            </div>
            <!-- END Skills checkboxes -->

            <div class="col-sm-12">
              <div class="form-group">
                <textarea name="message" class="form-control" cols="30" rows="10" placeholder="Project Descriptions" data-validation="required"></textarea>
              </div>
            </div>

            <div class="col-sm-12">
              <div class="form-group">
                <div class=" captcha">
                  <label>What is the sum of <?=$_SESSION['captcha'][0]?> + <?=$_SESSION['captcha'][1]?> ? <small><i>(Security question)</i></small></label>
                  <input type="text" class="form-control" name="captcha" data-validation="spamcheck" data-validation-captcha="<?=( $_SESSION['captcha'][0] + $_SESSION['captcha'][1] )?>" />
                </div>
              </div>
            </div>

            <div class="col-sm-12">
              <button class="btn btn-brand brand-color" data-hover="Submit" type="submit">Submit</button>
            </div>
          </form>
        </div>
      </section>