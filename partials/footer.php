<footer id="footer">
	<div class="container">
		<div class="row">
			<div class="col-sm-6 contact-info">
				<h4>Blue Sparrow Web Studio</h4>
				<p>hello@bluesparrowstudio.com</p>
				<p>www.bluesparrowstudio.com</p>

				<div class="social-icons">
					<ul class="list-unstyled list-inline">
						<li><a href="https://www.facebook.com/BlueSparrowWebStudio" target="_blank"><span class="fa fa-facebook"></span></a></li>
						<li><a href="https://plus.google.com/117121373496941382287" target="_blank"><span class="fa fa-google-plus"></span></a></li>
						<li><a href="https://twitter.com/bluesparrowWS" target="_blank"><span class="fa fa-twitter"></span></a></li>
						<li><a href="#"><span class="fa fa-pinterest"></span></a></li>
						<li><a href="https://www.behance.net/BlueSparrowWebStudio" target="_blank"><span class="fa fa-behance"></span></a></li>
					</ul>
				</div>

				<div class="row">
					<div class="col-sm-8 col-xs-12">
						<a href="quote" class="btn btn-block btn-brand" data-hover="Start a project">Start a project</a>
					</div>
				</div>

			</div>
			<!-- END Contact info -->

			<div class="col-sm-6 footer-contact">
				<h4 class="text-right">We'd love to hear from you.</h4>
				<form id="hola" action="/" method="post">
					<div class="clearfix">
						<div class="row">
							<div class="col-sm-6">
								<div class="form-group">
									<input type="text" class="form-control" name="name" placeholder="Your name" data-validation="required">
								</div>
							</div>
							<!-- END Your Name -->

							<div class="col-sm-6">
								<div class="form-group">
									<input type="email" class="form-control" name="email" placeholder="Your email" data-validation="email">
								</div>
							</div>
						</div>
					</div>
					<!-- END Your Email -->

					<div class="form-group">
						<textarea cols="30" rows="10" class="form-control" name="message" placeholder="Your message" data-validation="required"></textarea>
					</div>
					<!-- END Message -->

					<div class="form-group">
						<button class="btn btn-brand" data-hover="Submit">Submit</button>
					</div>
					<!-- END Submit button -->
				</form>
			</div>
			<!-- END Footer contact form -->
		</div>
	</div>
	<!-- END Container -->
</footer>