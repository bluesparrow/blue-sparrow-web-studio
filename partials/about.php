<section id="about" class="parallax" data-speed="0.2">
	<div class="container">
		<div class="row">
			<div class="col-sm-8 col-sm-offset-2 text-center">
				<h2 class="title">Branding, Logo design, <br> web design and development agency</h2>
				
				<p>We are a team of awesome designers and developers with over 5 years 
				of experience on graphics design, Web design and development.</p>

				<p>We specialise in Website design, 
				PSD to HTML conversion, WordPress, Front End development, 
				UI/UX design, Logo design, Branding and Graphics design.</p>
			</div>
		</div>
	</div>
</section>