<section id="works">
	<div class="container">
		<h2 class="title">Our works</h2>

		<div class="work-items row">
			<div class="col-lg-3 col-sm-4 item-wrapper">
				<div class="item brand-geeks">
					<figure class="image-holder">
						<img src="images/geeksonrepair.png" alt="" class="img-responsive">
					</figure>

					<div class="overlay">
						<div class="info">
							<h4>Geeks On Repair</h4>
							<small>Website development / WordPress</small>
						</div>

						<a href="http://geeksonrepair.com" class="btn btn-brand" data-hover="Visit Website">Visit website</a>
					</div>
					<!-- END overlay -->
				</div>
				<!-- END item -->
			</div>

			<div class="col-lg-3 col-sm-4 item-wrapper">
				<div class="item brand-benchmark">
					<figure class="image-holder">
						<img src="images/shengyi.jpg" alt="" class="img-responsive">
					</figure>

					<div class="overlay">
						<div class="info">
							<h4>Business Benchmark</h4>
							<small>Website design / WordPress</small>
						</div>

						<a href="http://188shengyi.com" class="btn btn-brand" data-hover="Visit Website">Visit website</a>
					</div>
					<!-- END overlay -->
				</div>
				<!-- END item -->
			</div>

			<div class="col-lg-3 col-sm-4 item-wrapper">
				<div class="item">
					<figure class="image-holder">
						<img src="images/orbis.png" alt="" class="img-responsive">
					</figure>

					<div class="overlay">
						<div class="info">
							<h4>Orbis Printing</h4>
							<small>Website design / HTML5 &amp; CSS3</small>
						</div>

						<a href="http://orbisprinting.com" class="btn btn-brand" data-hover="Visit Website">Visit website</a>
					</div>
					<!-- END overlay -->
				</div>
				<!-- END item -->
			</div>
			
			<div class="col-lg-3 col-sm-4 item-wrapper">
				<div class="item brand-enshored">
					<figure class="image-holder">
						<img src="images/enshored.png" alt="" class="img-responsive">
					</figure>

					<div class="overlay">
						<div class="info">
							<h4>Enshored</h4>
							<small>Website design / WordPress</small>
						</div>

						<a href="http://enshored.com" class="btn btn-brand" data-hover="Visit Website">Visit website</a>
					</div>
					<!-- END overlay -->
				</div>
				<!-- END item -->
			</div>

			<div class="col-lg-3 col-sm-4 item-wrapper">
				<div class="item">
					<figure class="image-holder">
						<img src="images/eventkloud.png" alt="" class="img-responsive">
					</figure>

					<div class="overlay">
						<div class="info">
							<h4>Eventkloud</h4>
							<small>PSD to HTML</small>
						</div>

						<a href="#" class="btn btn-brand" data-hover="Visit Website">Visit website</a>
					</div>
					<!-- END overlay -->
				</div>
				<!-- END item -->
			</div>

			<div class="col-lg-3 col-sm-4 item-wrapper">
				<div class="item brand-ecoart">
					<figure class="image-holder">
						<img src="images/ecoart.png" alt="" class="img-responsive">
					</figure>

					<div class="overlay">
						<div class="info">
							<h4>Ecoart Travel</h4>
							<small>Front End Development</small>
						</div>

						<a href="http://ecoarttravel.com" class="btn btn-brand" data-hover="Visit Website">Visit website</a>
					</div>
					<!-- END overlay -->
				</div>
				<!-- END item -->
			</div>
			
			<div class="col-lg-3 col-sm-4 item-wrapper">
				<div class="item brand-8a">
					<figure class="image-holder">
						<img src="images/8a.png" alt="" class="img-responsive">
					</figure>

					<div class="overlay">
						<div class="info">
							<h4>8a</h4>
							<small>Front End Development</small>
						</div>

						<a href="http://routes.8a.nu" class="btn btn-brand" data-hover="Visit Website">Visit website</a>
					</div>
					<!-- END overlay -->
				</div>
				<!-- END item -->
			</div>

			<div class="col-lg-3 col-sm-4 item-wrapper">
				<div class="item brand-privydate">
					<figure class="image-holder">
						<img src="images/privydate.png" alt="" class="img-responsive">
					</figure>

					<div class="overlay">
						<div class="info">
							<h4>Privy Date</h4>
							<small>PSD to HTML</small>
						</div>

						<a href="#" class="btn btn-brand" data-hover="Visit Website">Visit website</a>
					</div>
					<!-- END overlay -->
				</div>
				<!-- END item -->
			</div>

		</div>
		<!-- END Work items -->
	</div>
	<!-- END Container -->
</section>