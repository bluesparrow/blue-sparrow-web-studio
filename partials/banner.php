<div id="banner" class="container text-center">
	<div class="logo-holder">
		<img src="images/logo.png" alt="Blue Sparrow Web Studio">
	</div>
	<!-- END Logo holder -->

	<h2>
		<span class="agency-name animated pulse">Blue sparrow web studio</span>
		<span class="banner-text">Creative Agency</span>
	</h2>

	<a href="#works" class="btn btn-brand animated fadeIn" data-scroll data-hover="View our work">View our work</a>
</div>
<!-- END Banner -->