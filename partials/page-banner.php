<div class="banner-content">
	<div class="container">
		<div class="logo-holder">
			<img src="images/logo.png" alt="Blue Sparrow Web Studio" class="animated bounceInDown" width="200">
		</div>
		<!-- END Logo holder -->
		
		<div class="summary">
			<p>So you have a project? <br> That’s great, Let us know how we can help you.</p>
		</div>

		<div class="callout">Start your project</div>
	</div>
</div>