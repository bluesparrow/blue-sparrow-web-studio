<div class="preload">
      <div class="content">
        <img src="images/logo.png" alt="Blue Sparrow Web Studio" class="animated pulse infinite">

        <div class="spinner">
				  <div class="bounce1"></div>
				  <div class="bounce2"></div>
				  <div class="bounce3"></div>
				</div>
      </div>
      <!-- END Logo holder -->
    </div>
    <!-- END Preload -->