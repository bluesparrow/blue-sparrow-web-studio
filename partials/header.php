<header id="header">
	<nav class="navbar navbar-default navbar-fixed-top">
		<div class="container">
			<!-- Brand and toggle get grouped for better mobile display -->
			<div class="navbar-header">
				<button type="button" class="menu-btn navbar-toggle">
					<span class="sr-only">Toggle navigation</span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
				</button>
				
				<a class="navbar-brand" href="/">
					<span class="brand-name">Blue Sparrow Web Studio</span> <br> 
					<small class="slogan">a creative agency</small>
				</a>
			</div>
		
			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="hidden-xs">
				<ul class="nav navbar-nav navbar-right">
					<li><a data-scroll href="#">Home</a></li>
	        <li><a data-scroll href="#about">About us</a></li>
	        <li><a data-scroll href="#services">Our services</a></li>
	        <li><a data-scroll href="#testimonials">Testimonials</a></li>
					<li class="hidden-xs"><a href="quote" class="btn btn-sm btn-brand" data-hover="Start a project">Start a project</a></li>
				</ul>
			</div><!-- /.navbar-collapse -->
		</div>
	</nav>
</header>
<!-- END header -->