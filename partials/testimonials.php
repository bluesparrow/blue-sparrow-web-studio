<section id="testimonials" class="parallax" data-speed="0.2">
	<div class="container">
		<div class="col-sm-8 col-sm-offset-2">
			<div class="testimonials-wrapper">
				<blockquote class="item">
					<p>Bijay was able to create high class, 
					beautiful web pages with clean code in a timely manner and according to my instructions. 
					He was also able to offer other ideas that helped improved the overall layout and look of the site. 
					He was always online which made working with him easy and quick. 
					Thank you for everything so far Bijay! I look forward to working with you more in the near future. </p>

					<cite>
						- Anna Dang
					</cite>
				</blockquote>

				<blockquote class="item">
					<p>Manoj is a good designer who posses good knowledge of Photoshop and HTML5. 
					His communication and responsiveness were top-notch and his skills were reasonably strong. 
					I enjoyed working with him and will likely have additional jobs for him in the future.</p>

					<cite>
						- Yogesh Mathur, Lucid Solutions
					</cite>
				</blockquote>

				<blockquote class="item">
					<p>Bijay did a great job rebranding our website. 
					He was always around, patient, and knew how to tackle the technical stuff we paid him for. 
 					I reviewed a lot of others prior to hiring him and will use him going forward for more work.</p>
					
					<cite>
						- Ian Jackson, Enshored
					</cite>
				</blockquote>
				
				<blockquote class="item">
					<p>Manoj was very available and always gave 110%. I was very happy with his work.</p>

					<cite>
						- Kimberly Ella, 3clicks Travel Marketing
					</cite>
				</blockquote>

			</div>
			<!-- END Testimonials wrapper -->
		</div>
	</div> 
	<!-- END Container -->
</section>