<section id="services">
	<div class="container">
		<h2 class="title">Our Services</h2>
		
		<div class="row services-items">
			<article class="item col-sm-6">
				<div class="icon-wrapper">
					<span class="icon">
						<i class="fa fa-desktop"></i>	
					</span>
				</div>

				<div class="content">
					<h4 class="title">Web design</h4>
					<p>Your online presence makes a big different on your business. 
					Blue Sparrow understands your business value. 
					We build a modern and engaing website that speaks to your customers.</p>
				</div>
			</article>
			<!-- END Web design -->

			<article class="item col-sm-6">
				<div class="icon-wrapper">
					<span class="icon">
						<i class="fa fa-paint-brush"></i>	
					</span>
				</div>

				<div class="content">
					<h4 class="title">Graphic design</h4>
					<p>Every business needs a brand identity that speaks to their customers. 
					From logo design to letterheads, brochures and everything in between, we got you covered. </p>
				</div>
			</article>
			<!-- END Graphic design -->

			<article class="item col-sm-6">
				<div class="icon-wrapper">
					<span class="icon">
						<i class="fa fa-code"></i>	
					</span>
				</div>

				<div class="content">
					<h4 class="title">Web Development</h4>
					<p>From a Content management system for a website to complex web applications, 
					we love developing your ideas into reality. We specialize in HTML5/CSS3 , WordPress and PHP.
					</p>
				</div>
			</article>
			<!-- END Web development -->

			<article class="item col-sm-6">
				<div class="icon-wrapper">
					<span class="icon">
						PS
					</span>
				</div>

				<div class="content">
					<h4 class="title">PSD to HTML</h4>
					<p>Are you a designer who wants to hire someone to code their PSD? 
					Or you are freelancer who needs someone to take care of the HTML and CSS.
					Just send us your PSD file and let us take care of the rest.
					</p>
				</div>
			</article>
			<!-- END PSD to HTML -->
		</div>
		<!-- END Service items -->
	</div>
	<!-- END Container -->
</section>