<section id="team">
	<div class="container">
		<h2 class="title">Our Team</h2>

		<div class="row">
			<div class="col-sm-6 item">
				<div class="col-sm-6 avatar">
					<div class="row">
						<figure class="img-holder">
							<img src="images/bijay.jpg" alt="">
						</figure>
					</div>
				</div>
				<!-- END Avatar -->
				
				<div class="col-sm-6 info">
					<div class="row">
						<h4>
							<span class="name">Bijay Pakhrin</span>
							<small>Co-founder - Designer and Developer</small>
						</h4>

						<p class="quote">“If you want to draw water you do not dig six one-foot wells. You dig one six-foot well.” - Buddha</p>

						<ul class="list-unstyled list-inline social">
							<li><a href="http://twitter.com/bijaypakhrin" target="_blank"><span class="fa fa-twitter"></span></a></li>
							<li><a href="skype:monkeytempal?chat" target="_blank"><span class="fa fa-skype"></span></a></li>
							<li><a href="https://np.linkedin.com/in/bijaypakhrin" target="_blank"><span class="fa fa-linkedin"></span></a></li>
							<li><a href="https://www.behance.net/bijaypakhrin" target="_blank"><span class="fa fa-behance"></span></a></li>
							<li><a href="http://bijaypakhrin.me" target="_blank"><span class="fa fa-globe"></span></a></li>
							<li><a href="mailto:bijay@bluesparrowstudio.com"><span class="fa fa-envelope"></span></a></li>
						</ul>
					</div>
				</div>
				<!-- END Info -->
			</div>
			<!-- END item -->

			<div class="col-sm-6 item">
				<div class="col-sm-6 avatar">
					<div class="row">
						<figure class="img-holder">
							<img src="images/manoj.jpg" alt="">
						</figure>
					</div>
				</div>
				<!-- END Avatar -->
				
				<div class="col-sm-6 info odd">
					<div class="row">
						<h4>
							<span class="name">Manoj Shahi</span>
							<small>Co-founder - Front End Developer</small>
						</h4>

						<p class="quote">"If you can dream it, you can do it" - Walt Disney</p>

						<ul class="list-unstyled list-inline social">
							<li><a href="http://twitter.com/manozshahi" target="_blank"><span class="fa fa-twitter"></span></a></li>
							<li><a href="skype:manoz.shahi?chat" target="_blank"><span class="fa fa-skype"></span></a></li>
							<li><a href="https://np.linkedin.com/pub/manoj-shahi/82/a0b/193" target="_blank"><span class="fa fa-linkedin"></span></a></li>
							<li><a href="http://manojshahi.com.np" target="_blank"><span class="fa fa-globe"></span></a></li>
							<li><a href="mailto:manoj@bluesparrowstudio.com"><span class="fa fa-envelope"></span></a></li>
						</ul>
					</div>
				</div>
				<!-- END Info -->
			</div>
			<!-- END item -->

		</div>
		<!-- END ROW -->
	</div>
	<!-- END Container -->
</section>