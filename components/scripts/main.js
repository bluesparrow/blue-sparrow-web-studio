$(function(){
	// Display menu background on scroll
	$(document).scroll(function(){

    // Remove animate class added by preload
    $("#mainframe").removeClass("animated zoomIn");

		if($(document).scrollTop() > 120){
			$('.navbar-default').addClass('has-bg');
		}else {
			$('.navbar-default').removeClass('has-bg');
		}
	});

  $('.testimonials-wrapper').owlCarousel({
    pagination : true,
    singleItem:true
  });

  smoothScroll.init({
    offset: 90,
    easing: 'easeOutCubic'
  });

  // Form validation
  $.validate({
    form: '#hola',
    borderColorOnError : 'red',
    validateOnBlur: false,
    onValidate: function(){
      $('#mainframe').addClass("validate");
    },
    onSuccess : function() {
      $('#hola .btn').addClass("sending-light").text("Sending ...");
    }
  });

  $.validate({
    form: '#quote',
    borderColorOnError : 'red',
    modules : 'security',
    onSuccess : function() {
      $('#quote .btn').addClass("sending").text("Sending ...");
    }
  });

  // Hide Fixed Navbar on mobile while typing in form
  $('.navbar-fixed-top').removeClass("hidden-xs");
  $('.form-control').focus(function() {
    $('.navbar-fixed-top').addClass("hidden-xs");
    $(document).bind('focusin.navbar-fixed-top click.navbar-fixed-top',function(e) {
        if ($(e.target).closest('.navbar-fixed-top, .form-control').length) return;
        $(document).unbind('.navbar-fixed-top');
        $('.navbar-fixed-top').removeClass("hidden-xs");
    });
  });

  // Slim Scroll
  $('#team .quote').slimScroll({
      height: '100px;'
  });
});

// Display Loading on page load
$(window).load(function(){
  $(".preload").fadeOut(900);
  $("#mainframe").addClass("animated zoomIn");
});

$(document).ready(function() {
	// Changing Banner text using array
  var items = ["Web Design", "Web Development", "Graphic Design", "PSD to HTML", "Creative Agency"],
      $text = $( '.banner-text' ),
      delay = 2; //seconds
  
  function loop ( delay ) {
      $.each( items, function ( i, elm ){
          $text.delay( delay*1E3).fadeOut();
          $text.queue(function(){
              $text.html( items[i] );
              $text.dequeue();
          });
          $text.fadeIn();
          $text.queue(function(){
              if ( i == items.length -1 ) {
                  loop(delay);   
              }
              $text.dequeue();
          });
      });
  }
  loop( delay );


  // Add span into button with class btn-brand
  $(".btn-brand").each(function(){
    $(this.firstChild).wrap('<span></span>');
  });

  $('.alert-msg .close').click(function(){
    $(".alert-msg").fadeOut(200);
  });
});


var isMobile = {
  Android: function() {
      return navigator.userAgent.match(/Android/i);
  },
  BlackBerry: function() {
      return navigator.userAgent.match(/BlackBerry/i);
  },
  iOS: function() {
      return navigator.userAgent.match(/iPhone|iPad|iPod/i);
  },
  Opera: function() {
      return navigator.userAgent.match(/Opera Mini/i);
  },
  Windows: function() {
      return navigator.userAgent.match(/IEMobile/i);
  },
  any: function() {
      return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
  }
};

if( !isMobile.any() ) {
  $(function(){
    $(".parallax").simpleParallax();
  });
}


// Initiate Owl Carousel on mobile only
function mobile() {
  var checkWidth = $(window).width();
  var works = $(".work-items");

  if (checkWidth > 767) {
    
    works.data('owlCarousel').destroy(); 
    works.removeClass('owl-carousel');

    works.addClass('row');
    $('.item-wrapper').addClass('col-lg-3 col-sm-4');

  } else if (checkWidth < 768) {

    works.owlCarousel({
      singleItem: true,
      pagination: true
    });

    works.removeClass('row');
    $('.item-wrapper').removeClass('col-lg-3 col-sm-4');

  }
}

$(document).ready(mobile);
$(window).resize(mobile);