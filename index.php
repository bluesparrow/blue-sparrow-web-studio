<?php 
  if(isset($_POST['name']) && isset($_POST['email']) && isset($_POST['message'])):
    $name = $_POST['name'];
    $email = $_POST['email'];
    $message = $_POST['message'];
    
    $message_body .= "Sender's Name: ".$name."\n";
    $message_body .= "Sender's Email: ".$email."\n";
    $message_body .= $message."\n\n";
    $message_body .= "IP Address: " .$_SERVER['REMOTE_ADDR'];
    
    $to = 'monkeytempal@gmail.com';
    $subject = 'Hola! From Blue Sparrow Homepage';
    $body = $message_body;
    $headers = 'From: '.$email;
    
    if(empty($name) && empty($email) && empty($message)){
      $error = '<strong>Error - </strong> Please fill in all the fields';  
    }elseif(!filter_var($email, FILTER_VALIDATE_EMAIL)){
      $error = '<strong>Error - </strong> Please use valid email address'; 
    }else{
      mail($to, $subject, $body, $headers);
      $success = 'Your message has been sent, we will get back to you as soon as possible.';
    }
  endif;
?>

<!doctype html>
<html class="no-js" lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<title>Blue Sparrow web studio</title>
		<meta name="description" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1">

		<!-- Place favicon icons -->
		<link rel="icon" type="image/x-icon" href="images/favicon.ico" />
		<link rel="icon" type="image/png" href="images/touch-icon-iphone.png" />
		<link rel="apple-touch-icon" href="images/touch-icon-iphone.png">
		<link rel="apple-touch-icon" sizes="76x76" href="images/touch-icon-76.png">
		<link rel="apple-touch-icon" sizes="120x120" href="images/touch-icon-120.png">
		<link rel="apple-touch-icon" sizes="152x152" href="images/touch-icon-152.png">
		<link rel="apple-touch-icon" sizes="180x180" href="images/touch-icon-iphone-180.png">

		<!-- Font and Dependencies -->
		<link rel="stylesheet" href="css/bootstrap.min.css">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
		<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Source+Sans+Pro:400,400italic,700,700italic,300,300italic">
		<link rel="stylesheet" href="css/plugins.min.css">

		<!-- Custom styles -->
		<link rel="stylesheet" href="css/styles.css">

		<!-- Initiate Modernizr -->
		<script async src="js/vendor/modernizr-2.8.3.min.js"></script>
	</head>

  <body>
    <?php if(isset($error)){ 
      echo '<div class="alert-msg error">
              <div class="container">
                '.$error.'
                <span class="close">&times;</span>
              </div>
            </div>';
    }
    ?>

    <?php if(isset($success)){ 
      echo '<div class="alert-msg success">
              <div class="container">
                '.$success.'
                <span class="close">&times;</span>
              </div>
            </div>';
    }
    ?>

    <?php include('partials/offcanvas-nav.php'); ?>

    <div id="mainframe" class="push">
      <div id="wrapper">
        <div id="banner-wrapper" class="parallax" data-speed="0.5">
          <?php 
            include('partials/header.php'); 
            include('partials/banner.php');
          ?>
        </div>
        <!-- END Banner wrapper -->

        <?php include('partials/services.php'); ?>
        <!-- END Expertise -->

        <?php include('partials/about.php'); ?>
        <!-- END About -->

        <?php include('partials/works.php'); ?>
        <!-- END Works -->

        <?php include('partials/team.php'); ?>
        <!-- END Team -->

        <?php include('partials/testimonials.php'); ?>
        <!-- END Testimonials -->

        <div class="hero-banner">
          <div class="container">
            <div class="row">
              <div class="col-sm-9 col-lg-10">
                <p>Let's make something awesome</p>
              </div>

              <div class="col-sm-3 col-lg-2">
                <a href="quote" class="btn btn-block btn-brand brand-color" data-hover="Hire us now">Hire us now</a>
              </div>
            </div>
          </div>
        </div>
        <!-- END Hero banner -->
      </div>
      <!-- END Wrapper -->

      <?php include('partials/footer.php'); ?>
      <!-- END Footer -->
    </div>
    <!-- END Mainframe -->

    <?php include('partials/preload.php'); ?>
    <!-- END Preloader -->

    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="js/vendor/jquery.min.js"><\/script>')</script>

    <!-- Bootstrap JS -->
    <script src="js/bootstrap.min.js"></script>
    <script src="js/plugins.min.js"></script>

    <!-- Custom Scripts -->
    <script src="js/main.js"></script>
  </body>
</html>